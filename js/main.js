function clickButton(name) {
    let element = document.querySelector(name);
    element.onclick = (el) => {
        el.preventDefault();
        let topBlock =  document.querySelector('.hidden-block');
        if(topBlock.classList.length > 1){
            topBlock.classList = 'hidden-block';
            element.classList = 'burger';
        }
        else{
            topBlock.classList = 'hidden-block open';
            element.classList = 'burger active';
        }


    }
}
function slider (slider){
    let block = document.querySelector(slider);
    block.setAttribute('class','slider slider-active');
    let innerChildren = document.querySelector('.inner-slider');
    innerChildren.style.width = innerChildren.childElementCount * parseInt(block.clientWidth) + 'px';
    let  i = 0;
    if(i === 0){
        innerChildren.children[i].style.transform = 'translate(0,0)';
        innerChildren.children[i+1].style.transform = 'translate(-100%,0)';
        i = 1;
    }
    setInterval(function () {
             if(i === 1){
                innerChildren.children[i].style.transform = 'translate(0,0)';
                innerChildren.children[i-1].style.transform = 'translate(-100%,0)';

            }
            else if(i === 2){
                i = 0;
                innerChildren.children[i].style.transform = 'translate(0,0)';
                innerChildren.children[i+1].style.transform = 'translate(100%,0)';

            }
        i += 1;
        console.log(i)
    },5000)
}


clickButton('.burger');
slider('.slider');